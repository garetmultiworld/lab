﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using Cinemachine;
using UnityEngine.Events;

namespace MoreMountains.CorgiEngine
{
    /// <summary>
    /// This class lets you define the boundaries of rooms in your level.
    /// Rooms are useful if you want to cut your level into portions (think Super Metroid or Hollow Knight for example).
    /// These rooms will require their own virtual camera, and a confiner to define their size. 
    /// Note that the confiner is different from the collider that defines the room.
    /// You can see an example of rooms in action in the RetroVania demo scene.
    /// </summary>
    public class Room : MonoBehaviour, MMEventListener<CorgiEngineEvent>
    {
        /// the collider for this room
        public Collider2D RoomCollider { get { return _roomCollider2D; } }

        [Header("Camera")]
        /// the virtual camera associated to this room
        public CinemachineVirtualCamera VirtualCamera;
        /// the confiner for this room, that will constrain the virtual camera, usually placed on a child object of the Room
        public Collider2D Confiner;
        /// the confiner component of the virtual camera
        public CinemachineConfiner CinemachineCameraConfiner;
        /// whether or not the confiner should be auto resized on start to match the camera's size and ratio
        public bool ResizeConfinerAutomatically = true;
        /// whether or not this Room should look at the level's start position and declare itself the current room on start or not
        public bool AutoDetectFirstRoomOnStart = true;

        [Header("History")]
        /// whether this room has already been visited or not
        public bool RoomVisited = false;

        [Header("Actions")]
        /// the event to trigger when the player enters the room for the first time
        public UnityEvent OnPlayerEntersRoomForTheFirstTime;
        /// the event to trigger everytime the player enters the room
        public UnityEvent OnPlayerEntersRoom;
        /// the event to trigger everytime the player exits the room
        public UnityEvent OnPlayerExitsRoom;

        protected Collider2D _roomCollider2D;
        protected Camera _mainCamera;
        protected UnityEngine.U2D.PixelPerfectCamera test;
        protected Vector2 _cameraSize;
        
        /// <summary>
        /// On Start we initialize our room
        /// </summary>
        protected virtual void Start()
        {
            Initialization();
        }

        /// <summary>
        /// Grabs our Room collider, our main camera, and starts the confiner resize
        /// </summary>
        protected virtual void Initialization()
        {
            _roomCollider2D = this.gameObject.GetComponent<Collider2D>();
            _mainCamera = Camera.main;
            StartCoroutine(ResizeConfiner());
        }

        /// <summary>
        /// Resizes the confiner 
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator ResizeConfiner()
        {
            if ((VirtualCamera == null) || (Confiner == null) || !ResizeConfinerAutomatically)
            {
                yield return null;
            }

            // we wait two more frame for Unity's pixel perfect camera component to be ready because apparently sending events is not a thing.
            yield return null;
            yield return null;

            (Confiner as BoxCollider2D).offset = _roomCollider2D.offset;
            (Confiner as BoxCollider2D).size = (_roomCollider2D as BoxCollider2D).size;

            _cameraSize.y = 2 * _mainCamera.orthographicSize;
            _cameraSize.x = _cameraSize.y * _mainCamera.aspect;

            Vector2 newSize = (Confiner as BoxCollider2D).size;

            if ((Confiner as BoxCollider2D).size.x < _cameraSize.x)
            {
                newSize.x = _cameraSize.x;
            }
            if ((Confiner as BoxCollider2D).size.y < _cameraSize.y)
            {
                newSize.y = _cameraSize.y;
            }

            (Confiner as BoxCollider2D).size = newSize;
            CinemachineCameraConfiner.InvalidatePathCache();

            HandleLevelStartDetection();
        }   
        
        /// <summary>
        /// Looks for the level start position and if it's inside the room, makes this room the current one
        /// </summary>
        protected virtual void HandleLevelStartDetection()
        {
            if (AutoDetectFirstRoomOnStart)
            {
                if (LevelManager.Instance != null)
                {
                    if (_roomCollider2D.bounds.Contains(LevelManager.Instance.CurrentCheckPoint.transform.position))
                    {
                        MMCameraEvent.Trigger(MMCameraEventTypes.ResetPriorities);
                        MMCinemachineBrainEvent.Trigger(MMCinemachineBrainEventTypes.ChangeBlendDuration, 0f);
                        VirtualCamera.Priority = 10;

                        MMSpriteMaskEvent.Trigger(MMSpriteMaskEvent.MMSpriteMaskEventTypes.MoveToNewPosition,
                            (Vector2)_roomCollider2D.bounds.center + _roomCollider2D.offset,
                            _roomCollider2D.bounds.size,
                            0f, MMTween.MMTweenCurve.LinearTween);

                        PlayerEntersRoom();
                    }
                }
            }
        }

        /// <summary>
        /// Call this to let the room know a player entered
        /// </summary>
        public virtual void PlayerEntersRoom()
        {
            if (RoomVisited)
            {
                OnPlayerEntersRoom?.Invoke();
            }
            else
            {
                RoomVisited = true;
                OnPlayerEntersRoomForTheFirstTime?.Invoke();
            }            
        }

        /// <summary>
        /// Call this to let this room know a player exited
        /// </summary>
        public virtual void PlayerExitsRoom()
        {
            OnPlayerExitsRoom?.Invoke();
        }

        /// <summary>
        /// When we get a respawn event, we ask for a camera reposition
        /// </summary>
        /// <param name="corgiEngineEvent"></param>
        public virtual void OnMMEvent(CorgiEngineEvent corgiEngineEvent)
        {
            if (corgiEngineEvent.EventType == CorgiEngineEventTypes.Respawn)
            {
                HandleLevelStartDetection();
            }
        }

        /// <summary>
        /// On enable we start listening for events
        /// </summary>
        protected virtual void OnEnable()
        {
            this.MMEventStartListening<CorgiEngineEvent>();
        }

        /// <summary>
        /// On enable we stop listening for events
        /// </summary>
        protected virtual void OnDisable()
        {
            this.MMEventStopListening<CorgiEngineEvent>();
        }
    }
}
