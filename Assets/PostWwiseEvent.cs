﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostWwiseEvent : MonoBehaviour
{   
    public enum Eventos
    {
        Wooble, 
        Sword,
        Jump,
    }
    public AK.Wwise.Event Wooble;
    public AK.Wwise.Event Sword;
    public AK.Wwise.Event Jump;
    // Start is called before the first frame update
  
    public void PlaySfx(Eventos Sonido)
    {   
        switch (Sonido)
        {
            case Eventos.Wooble:
            Wooble.Post(gameObject);
            break;
            case Eventos.Sword:
            Sword.Post(gameObject);
            break;
            case Eventos.Jump:
            Jump.Post(gameObject);
            break;
        }
        
    }
 
   
}
