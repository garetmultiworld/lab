﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaMusic : MonoBehaviour
{

    [Header("On Enter")]
    public string OnEnterMusic;
    public bool OnEnterPlay;
    public bool OnEnterPlayWithFade;
    public bool OnEnterPlayWithCrossFade;
    public float OnEnterTransitionTime = 1;
    public float OnEnterWaitTime = 0;
    public float OnEnterPlayFrom = 0;

    [Header("On Exit")]
    public string OnExitMusic;
    public bool OnExitPlay;
    public bool OnExitPlayWithFade;
    public bool OnExitPlayWithCrossFade;
    public float OnExitTransitionTime = 1;
    public float OnExitWaitTime = 0;
    public float OnExitPlayFrom = 0;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player") && !OnEnterMusic.Equals(""))
        {
            if (OnEnterPlay)
            {
                AudioManager.Instance.PlayMusic(OnEnterMusic, OnEnterPlayFrom);
            }else if (OnEnterPlayWithFade)
            {
                AudioManager.Instance.PlayMusicWithFade(OnEnterMusic, OnEnterTransitionTime, OnEnterWaitTime, OnEnterPlayFrom);
            }
            else if (OnEnterPlayWithCrossFade)
            {
                AudioManager.Instance.PlayMusicWithCrossFade(OnEnterMusic, OnEnterTransitionTime, OnEnterPlayFrom);
            }
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player") && !OnExitMusic.Equals(""))
        {
            if (OnExitPlay)
            {
                AudioManager.Instance.PlayMusic(OnExitMusic, OnExitPlayFrom);
            }
            else if (OnExitPlayWithFade)
            {
                AudioManager.Instance.PlayMusicWithFade(OnExitMusic, OnExitTransitionTime, OnExitWaitTime, OnExitPlayFrom);
            }
            else if (OnExitPlayWithCrossFade)
            {
                AudioManager.Instance.PlayMusicWithCrossFade(OnExitMusic, OnExitTransitionTime, OnExitPlayFrom);
            }
        }
    }

}
