﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaMusicControlChannel : MonoBehaviour
{

    [Header("On Enter")]
    public string OnEnterChannel;
    public bool OnEnterStop;
    public bool OnEnterStopWithFade;
    public bool OnEnterPause;
    public bool OnEnterResume;
    public float OnEnterTransitionTime = 1;

    [Header("On Exit")]
    public string OnExitChannel;
    public bool OnExitStop;
    public bool OnExitStopWithFade;
    public bool OnExitPause;
    public bool OnExitResume;
    public float OnExitTransitionTime = 1;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player") && !OnEnterChannel.Equals(""))
        {
            if (OnEnterStop)
            {
                AudioManager.Instance.StopMusic(OnEnterChannel);
            }
            else if (OnEnterPause)
            {
                AudioManager.Instance.PauseMusic(OnEnterChannel);
            }
            else if (OnEnterResume)
            {
                AudioManager.Instance.ResumeMusic(OnEnterChannel);
            }
            else if (OnEnterStopWithFade)
            {
                AudioManager.Instance.StopWithFadeMusic(OnEnterChannel, OnEnterTransitionTime);
            }
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player") && !OnExitChannel.Equals(""))
        {
            if (OnEnterStop)
            {
                AudioManager.Instance.StopMusic(OnExitChannel);
            }
            else if (OnEnterPause)
            {
                AudioManager.Instance.PauseMusic(OnExitChannel);
            }
            else if (OnEnterResume)
            {
                AudioManager.Instance.ResumeMusic(OnExitChannel);
            }
            else if (OnExitStopWithFade)
            {
                AudioManager.Instance.StopWithFadeMusic(OnEnterChannel, OnExitTransitionTime);
            }
        }
    }

}
