﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaSoundFx : MonoBehaviour
{

    [Header("On Enter")]
    public string OnEnterSound;
    [Range(0f,100f)]
    public float OnEnterChances = 100f;

    [Header("On Exit")]
    public string OnExitSound;
    [Range(0f, 100f)]
    public float OnExitChances = 100f;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player") && !OnEnterSound.Equals(""))
        {
            if (Random.Range(0f, 100f) <= OnEnterChances)
            {
                AudioManager.Instance.PlaySfx(OnEnterSound, this.gameObject);
            }
        }
    }

    public void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player") && !OnExitSound.Equals(""))
        {
            if (Random.Range(0f, 100f) <= OnExitChances)
            {
                AudioManager.Instance.PlaySfx(OnExitSound, this.gameObject);
            }
        }
    }

}
