﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMusicChannelTrigger : TriggerInterface
{

    public string MusicName;
    public string DestinationChannel;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        AudioManager.Instance.ChangeMusicChannel(MusicName, DestinationChannel);
    }

}
