﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicChannelVolumeTrigger : TriggerInterface
{

    public string Channel;
    [Range(0f, 1f)]
    public float Volume;
    public float TransitionTime=1;

    protected bool IsPlaying;
    protected float Speed;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        float prevVolume = AudioManager.Instance.GetMusicChannelVolume(Channel);
        float diff;
        if (prevVolume < Volume)
        {
            diff = Volume-prevVolume;
            Speed = diff / TransitionTime;
        }
        else
        {
            diff = prevVolume-Volume;
            Speed = -1 * diff / TransitionTime;
        }
        StartCoroutine(UpdateVolume(prevVolume));
    }

    private IEnumerator UpdateVolume(float prevVolume)
    {
        float time;
        float currentVolume = prevVolume;
        for (time = 0; time < TransitionTime; time += Time.deltaTime)
        {
            currentVolume += Speed * Time.deltaTime;
            AudioManager.Instance.SetMusicChannelVolume(Channel,currentVolume);
            yield return null;
        }
    }

}
