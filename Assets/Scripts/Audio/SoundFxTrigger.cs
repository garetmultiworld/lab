﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFxTrigger : TriggerInterface
{

    public string Sound;
    [Range(0f, 100f)]
    public float Chances = 100f;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (Random.Range(0f, 100f) <= Chances)
        {
            AudioManager.Instance.PlaySfx(Sound, this.gameObject);
        }
    }

    public override void Cancel()
    {
    }

}
