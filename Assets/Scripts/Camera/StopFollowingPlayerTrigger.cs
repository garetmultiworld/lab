﻿using Cinemachine;
using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopFollowingPlayerTrigger : TriggerInterface
{

    public Transform NewFollow;
    public CinemachineVirtualCamera cinemachineVirtualCamera;
    public CinemachineCameraController cinemachineCameraController;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        cinemachineCameraController.StopFollowing();
        cinemachineVirtualCamera.Follow = NewFollow;
    }
}
