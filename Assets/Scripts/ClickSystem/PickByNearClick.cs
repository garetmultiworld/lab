﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickByNearClick : MonoBehaviour
{

    CircleCollider2D _playerRadius=null;
    InventoryPickableItem _inventoryPickableItem;

    void Start()
    {
        _playerRadius = GetComponent<CircleCollider2D>();
        _inventoryPickableItem = GetComponent<InventoryPickableItem>();
    }

    void OnMouseDown()
    {
        if (_playerRadius != null)
        {
            Vector2 center = new Vector2(_playerRadius.transform.position.x, _playerRadius.transform.position.y) + _playerRadius.offset;

            Collider2D item = Physics2D.OverlapCircle(center, PlayerConfig.GetInstance().EnvironmentDetectionRadius, 1 << LayerMask.NameToLayer("Player"));

            if (item != null)
            {
                _inventoryPickableItem.Pick();
                Destroy(gameObject);
            }
        }
    }

}
