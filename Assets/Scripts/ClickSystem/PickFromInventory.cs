﻿using MoreMountains.CorgiEngine;
using MoreMountains.InventoryEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PickFromInventory : MonoBehaviour, MMEventListener<MMInventoryEvent>
{

    InventorySlot _inventorySlot;

    public void OnMMEvent(MMInventoryEvent eventType)
    {
        if(
            eventType.Slot!=null&&
            eventType.Slot.Index==_inventorySlot.Index&&
            eventType.InventoryEventType== MMInventoryEventType.Click
        )
        {
            if (eventType.EventItem != null)
            {
                if (eventType.EventItem.IsEquippable)
                {
                    eventType.Slot.Equip();
                    MMInventoryEvent.Trigger(MMInventoryEventType.InventoryCloseRequest, _inventorySlot, _inventorySlot.ParentInventoryDisplay.TargetInventoryName, null, 0, 0);
                }
                else
                {
                    switch (eventType.EventItem.ItemID)
                    {
                        case "Jupiter":
                        case "Marte":
                        case "Mercurio":
                        case "Neptuno":
                        case "Pluton":
                        case "Saturno":
                        case "Tierra":
                        case "Urano":
                        case "Venus":
                            GameObject pickedObject = Instantiate(eventType.EventItem.Prefab, new Vector3(0, 0, 0), Quaternion.identity);
                            pickedObject.GetComponent<PickByNearClick>().enabled = false;
                            pickedObject.GetComponent<InventoryPickableItem>().enabled = false;
                            PlaceByNearClick placeByNearClick = pickedObject.GetComponent<PlaceByNearClick>();
                            placeByNearClick.enabled = true;
                            placeByNearClick.selectedSlot = _inventorySlot;
                            placeByNearClick.selectedItem = eventType.EventItem;
                            FollowMouse followMouse = pickedObject.GetComponent<FollowMouse>();
                            followMouse.enabled = true;
                            FollowMouse.selectedInstance = followMouse;
                            pickedObject.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Foreground");
                            MMInventoryEvent.Trigger(MMInventoryEventType.InventoryCloseRequest, _inventorySlot, _inventorySlot.ParentInventoryDisplay.TargetInventoryName, null, 0, 0);
                            break;
                    }
                }
            }
        }
    }

    void Start()
    {
        _inventorySlot = GetComponent<InventorySlot>();
        MMEventManager.AddListener<MMInventoryEvent>(this);
    }

}
