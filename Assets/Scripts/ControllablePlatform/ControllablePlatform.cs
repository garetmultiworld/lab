﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class ControllablePlatform : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    public triggerControllablePlatform triggerControllablePlatform;
    public Rigidbody2D platformRb;
    public float horizontalSpeed = 3f;
    public float VerticalSpeed = 3f;
    public bool iscolliding = false;
    public RespawnPlatformCheck RespawnPlatform;
    private static float idleTime = 5f;
    private float idleCounter = idleTime;
    private bool flag;
    private bool activeFlag;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
       
        if (iscolliding)
        {
            activeFlag = true;
            idleCounter = idleTime;
            if (Input.GetKeyDown("e"))
            {
                flag = !flag;
                if (!flag)
                {
                    triggerControllablePlatform.state = 2;
                }
            }
            if (flag)
            {
                
                triggerControllablePlatform.state = 1;
                platformRb.velocity = new Vector2(Input.GetAxis("Player1_Horizontal") * horizontalSpeed, Input.GetAxis("Player1_Vertical") * VerticalSpeed);
              
            }
            else
            {
                platformRb.velocity = new Vector2(0f, 0f);
            }

        }
        else 
        {
            platformRb.velocity = new Vector2(0f, 0f);
            flag = false;
            if (activeFlag)
            {
                idleCounter = idleCounter - Time.deltaTime;
                if (idleCounter <= 0f)
                {
                    if (RespawnPlatform.canRespawn)
                    {
                        triggerControllablePlatform.state = 0;
                        activeFlag = false;
                        idleCounter = idleTime;
                        gameObject.transform.position = RespawnPlatform.transform.position;
                    }
                }
            }
            else 
            {
                gameObject.transform.position = RespawnPlatform.transform.position;
            }
        }
    }
}
