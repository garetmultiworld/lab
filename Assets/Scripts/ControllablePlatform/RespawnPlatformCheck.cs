﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnPlatformCheck : MonoBehaviour
{
    public bool canRespawn;
    private void OnTriggerStay2D(Collider2D collision)
    {
        canRespawn = false;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        canRespawn = true;
    }
        // Start is called before the first frame update
        void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
