﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

public class triggerControllablePlatform : MonoBehaviour
{
    public ControllablePlatform ControllablePlatform;
    public int state=0;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            ControllablePlatform.iscolliding = false;
        }
        else
        {
            ControllablePlatform.iscolliding = false;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag =="Player") {
            ControllablePlatform.iscolliding = true;
            if (state == 1)
            {
                collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterHorizontalMovement>().AbilityPermitted = false;
                //collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterHorizontalMovement>().WalkSpeed = 0;
                collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterJump>().AbilityPermitted = false;
            }
            else if (state == 2)
            {
                collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterHorizontalMovement>().AbilityPermitted = true;
                //collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterHorizontalMovement>().WalkSpeed = 6;
                collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterJump>().AbilityPermitted = true;
                state = 0;
            }
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ControllablePlatform.iscolliding = false;
            collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterHorizontalMovement>().AbilityPermitted = true;
            //collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterHorizontalMovement>().WalkSpeed = 6;
            collision.gameObject.GetComponent<MoreMountains.CorgiEngine.CharacterJump>().AbilityPermitted = true;
            state = 0;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      
    }
}
