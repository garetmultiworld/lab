﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIActionFireTrigger : AIAction
{

    public TriggerInterface Trigger;


    public override void OnExitState()
    {
        base.OnExitState();
        Trigger.Trigger();
    }

    public override void PerformAction()
    {
    }

}
