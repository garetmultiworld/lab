﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIActionFlyOtherTowardsTargetLastPosition : AIActionFlyTowardsTargetLastPosition
{
    public GameObject Other;

    protected override void Initialization()
    {
        _characterFly = Other.GetComponent<CharacterFly>();
        movable = Other;
    }

}
