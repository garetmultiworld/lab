﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIActionPatrolWithinBoundsImproved : AIActionPatrolWithinBounds
{

    [Header("Bounds Pause Chance")]
    [Range(0f, 100f)]
    public float PauseChances = 100f;
    public float MinPauseTime = 1f;
    public float MaxPauseTime = 1f;

    protected bool Stopped = false;
    protected bool Resuming = false;

    /// <summary>
    /// On Enter state we determine our bounds if required
    /// </summary>
    public override void OnEnterState()
    {
        base.OnEnterState();
        Stopped = false;
        Resuming = false;
    }

    /// <summary>
    /// Determines whether or not bounds have been exceeded and turns around if needed
    /// When turning, there's a chance to stop
    /// </summary>
    protected override void CheckForDistance()
    {
        if (Stopped || Resuming)
        {
            return;
        }
        if (this.transform.position.x < _boundsLeft.x)
        {
            _direction = Vector2.right;
            CheckBoundStop();
        }
        if (this.transform.position.x > _boundsRight.x)
        {
            _direction = Vector2.left;
            CheckBoundStop();
        }
    }

    protected virtual void CheckBoundStop()
    {
        if (Random.Range(0f, 100f) < PauseChances)
        {
            Stopped = true;
            StartCoroutine(WaitOnBounds(_direction));
            _direction = Vector2.zero;
        }
        else
        {
            Resuming = true;
            StartCoroutine(WaitOnResume());
        }
    }

    private IEnumerator WaitOnBounds(Vector2 newDirection)
    {
        float t = 0;
        float transitionTime = Random.Range(MinPauseTime, MaxPauseTime);
        for (t = 0; t < transitionTime && Stopped; t += Time.deltaTime)
        {
            yield return null;
        }
        Stopped = false;
        Resuming = true;
        _direction = newDirection;
        StartCoroutine(WaitOnResume());
    }

    private IEnumerator WaitOnResume()
    {
        float t = 0;
        float transitionTime = 1f;
        for (t = 0; t < transitionTime && Resuming; t += Time.deltaTime)
        {
            yield return null;
        }
        Resuming = false;
    }

}
