﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIDecisionList : ReorderableArray<AIDecision>
{
}

/// <summary>
/// Applies the AND logical gate to the chosen decisions
/// Meaning that if any of the decisions are false, this decision will also be false
/// If Nand is true, the result of this decision will be negated (true becomes false, false becomes true)
/// </summary>
public class AIDecisionAnd : AIDecision
{

    [Reorderable(null, "Decision", null)]
    public AIDecisionList Decisions;
    public bool Nand=false;

    public override void Initialization()
    {
        foreach (AIDecision decision in Decisions)
        {
            decision.Initialization();
        }
    }

    public override bool Decide()
    {
        bool result = true;
        foreach (AIDecision decision in Decisions)
        {
            if (!decision.Decide())
            {
                result=false;
                break;
            }
        }
        if (Nand)
        {
            return !result;
        }
        else
        {
            return result;
        }
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        foreach (AIDecision decision in Decisions)
        {
            decision.OnEnterState();
        }
    }

    public override void OnExitState()
    {
        base.OnExitState();
        foreach (AIDecision decision in Decisions)
        {
            decision.OnExitState();
        }
    }

}
