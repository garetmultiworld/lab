﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDecisionArrivedAtDestination : AIDecision
{
    public float MinimumDistance = 1f;

    public GameObject Actor;
    public GameObject Destination;

    public override bool Decide()
    {
        bool arrived = true;

        if (!(Mathf.Abs(Actor.transform.position.x - Destination.transform.position.x) < MinimumDistance))
        {
            arrived = false;
        }

        if (!(Mathf.Abs(Actor.transform.position.y - Destination.transform.position.y) < MinimumDistance))
        {
            arrived = false;
        }

        return arrived;
    }

}
