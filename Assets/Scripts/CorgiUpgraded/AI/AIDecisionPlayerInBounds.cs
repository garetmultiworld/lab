﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDecisionPlayerInBounds: AIDecision
{

    public RectBounds Bounds;
    protected GameObject _player;
    protected float _startTime = 0f;
    protected Vector2 topLeft;
    protected Vector2 bottomRight;

    public override void Initialization()
    {
        _player=GameObject.Find("LevelManager").GetComponent<LevelManager>().Players[0].gameObject;
        topLeft = Bounds.GetTopLeft();
        bottomRight = Bounds.GetBottomRight();
    }

    public override bool Decide()
    {
        return EvaluateBounds();
    }

    protected virtual bool EvaluateBounds()
    {
        return 
            _player.transform.position.x >= topLeft.x &&
            _player.transform.position.x <= bottomRight.x &&
            _player.transform.position.y >= topLeft.y &&
            _player.transform.position.y <= bottomRight.y
            ;
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        _startTime = Time.time;
    }

}
