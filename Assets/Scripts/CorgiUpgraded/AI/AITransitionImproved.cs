﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AIStatesList : ReorderableArray<AIStateChoice>
{
}

[System.Serializable]
public class AITransitionImproved
{
    /// this transition's decision
    public AIDecision Decision;
    /// the states to transition to if this Decision returns true, if more than one it chooses randomly
    [Reorderable(null, "True State", null)]
    public AIStatesList TrueStates;
    /// the states to transition to if this Decision returns false, if more than one it chooses randomly
    [Reorderable(null, "False State", null)]
    public AIStatesList FalseStates;
}
