﻿using UnityEngine;

public class ElevatorTrigger : TriggerInterface
{

    public Elevator elevator;
    public bool Reverse=false;
    public bool SpecificPoint=false;
    public int PointIndex;

    public override void Cancel()
    {
        elevator.SetCanMove(false);
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (SpecificPoint)
        {
            elevator.MoveToIndex(PointIndex);
        }
        else if (Reverse)
        {
            elevator.MoveBackwards();
        }
        else
        {
            elevator.MoveForwards();
        }
    }
}
