﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{

    public bool IsOpen;
    public TriggerInterface TriggerOnOpen;
    public TriggerInterface TriggerOnClose;

    protected Collider2D[] colliders;

    void Start()
    {
        colliders = gameObject.GetComponents<Collider2D>();
        foreach(Collider2D coll in colliders)
        {
            coll.enabled = !IsOpen;
        }
    }

    public void Open()
    {
        if (IsOpen)
        {
            return;
        }
        IsOpen = true;
        foreach (Collider2D coll in colliders)
        {
            coll.enabled = false;
        }
        if (TriggerOnOpen != null)
        {
            TriggerOnOpen.Trigger();
        }
    }

    public void Close()
    {
        if (!IsOpen)
        {
            return;
        }
        IsOpen = false;
        foreach (Collider2D coll in colliders)
        {
            coll.enabled = true;
        }
        if (TriggerOnClose != null)
        {
            TriggerOnClose.Trigger();
        }
    }

}
