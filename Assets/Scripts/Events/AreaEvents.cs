﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AreaEventTypes
{
    Enter,
    Stay,
    Exit
}

public struct AreaEvent
{

    public Character TargetCharacter;
    public AreaEventTypes EventType;

    public AreaEvent(Character character, AreaEventTypes eventType)
    {
        TargetCharacter = character;
        EventType = eventType;
    }

    static AreaEvent e;
    public static void Trigger(Character character, AreaEventTypes eventType)
    {
        e.TargetCharacter = character;
        e.EventType = eventType;
        MMEventManager.TriggerEvent(e);
    }

}
