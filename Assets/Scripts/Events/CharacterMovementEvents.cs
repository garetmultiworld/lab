﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CharacterMovementEventTypes
{
    Flip
}

public struct CharacterMovementEvent
{

    public Character TargetCharacter;
    public CharacterMovementEventTypes EventType;

    public CharacterMovementEvent(Character character, CharacterMovementEventTypes eventType)
    {
        TargetCharacter = character;
        EventType = eventType;
    }

    static CharacterMovementEvent e;
    public static void Trigger(Character character, CharacterMovementEventTypes eventType)
    {
        e.TargetCharacter = character;
        e.EventType = eventType;
        MMEventManager.TriggerEvent(e);
    }

}
