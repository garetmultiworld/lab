﻿using MoreMountains.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerEventTypes
{
    ActivateDash,
    ActivateWalljump,
    ActivateDoublejump,
    ActivateBasicGun,
    DisableHorizontalMovement,
    EnableHorizontalMovement,
    DisableJump,
    EnableJump,
    DisableWeaponUse,
    EnableWeaponUse,
    Nothing,
    ChangeHorizontalMovementSpeed,
    DisableDoublejump,
    EnableTeslaGruaForm,
    DisableTeslaGruaForm
}

public struct PlayerEvent
{

    public PlayerEventTypes EventType;
    public AnimationParameterTrigger animationParameterTrigger;
    public float NewValue;

    public PlayerEvent(PlayerEventTypes eventType, AnimationParameterTrigger animParameterTrigger, float newValue)
    {
        EventType = eventType;
        animationParameterTrigger = animParameterTrigger;
        NewValue = newValue;
    }

    static PlayerEvent e;
    public static void Trigger(PlayerEventTypes eventType, AnimationParameterTrigger animParameterTrigger, float newValue)
    {
        e.EventType = eventType;
        e.animationParameterTrigger = animParameterTrigger;
        e.NewValue = newValue;
        MMEventManager.TriggerEvent(e);
    }

}