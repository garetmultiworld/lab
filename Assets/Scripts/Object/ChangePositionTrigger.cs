﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class ChangePositionTrigger: TriggerInterface
{

    public Transform NewPosition;
    public GameObject Object;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        Object.transform.position = NewPosition.position;
    }

}
