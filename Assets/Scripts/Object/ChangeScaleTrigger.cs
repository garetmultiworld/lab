﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeScaleTrigger : TriggerInterface
{

    public GameObject TheObject;

    public bool ScaleY = true;
    public float MinYScale;
    public float MaxYScale;
    public float MinTimeY=1;
    public float MaxTimeY=2;
    public AnimationCurve AnimCurveY;
    public TriggerInterface TriggerOnArrive;

    private bool IsAnimating = false;
    private float StartingScaleY;
    private float TargetScaleY;
    private float TimeY;
    private float CurrentTimerY = 0;

    public override void Cancel()
    {
        IsAnimating = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        if (ScaleY)
        {
            StartingScaleY = TheObject.transform.localScale.y;
            TargetScaleY = Random.Range(MinYScale, MaxYScale);
            TimeY = Random.Range(MinTimeY, MaxTimeY);
            CurrentTimerY = 0;
        }
        IsAnimating = true;
    }

    protected virtual void Update()
    {
        if (!IsAnimating)
        {
            return;
        }
        float t = CurrentTimerY / TimeY;
        TheObject.transform.localScale = new Vector3(
            TheObject.transform.localScale.x,
            StartingScaleY + ((TargetScaleY - StartingScaleY) * AnimCurveY.Evaluate(t)),
            TheObject.transform.localScale.z
        );
        CurrentTimerY += Time.deltaTime;
        if (CurrentTimerY >= TimeY)
        {
            TheObject.transform.localScale = new Vector3(
                TheObject.transform.localScale.x,
                TargetScaleY,
                TheObject.transform.localScale.z
            );
            IsAnimating = false;
            if (TriggerOnArrive != null)
            {
                TriggerOnArrive.Trigger();
            }
        }
    }

}
