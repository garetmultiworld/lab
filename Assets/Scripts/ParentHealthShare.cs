﻿using MoreMountains.CorgiEngine;
using MoreMountains.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentHealthShare : MonoBehaviour, MMEventListener<MMDamageTakenEvent>, MMEventListener<CharacterMovementEvent>
{

    public Health parentHealth;
    protected Character character;

    public void OnMMEvent(MMDamageTakenEvent damageTaken)
    {
        if(damageTaken.AffectedCharacter==null || damageTaken.AffectedCharacter.gameObject == null)
        {
            return;
        }
        if(damageTaken.AffectedCharacter.gameObject.GetInstanceID() == gameObject.GetInstanceID())
        {
            parentHealth.Invulnerable = false;
            parentHealth.Damage((int)damageTaken.DamageCaused,damageTaken.Instigator,0,0);
            parentHealth.Invulnerable = true;
        }
    }

    public void OnMMEvent(CharacterMovementEvent eventType)
    {
        if (eventType.TargetCharacter == null || eventType.TargetCharacter.gameObject == null)
        {
            return;
        }
        if (eventType.TargetCharacter.gameObject.GetInstanceID() == character.gameObject.GetInstanceID())
        {
            transform.localPosition = new Vector3(-transform.localPosition.x,transform.localPosition.y, transform.localPosition.z);
        }
    }

    void Awake()
    {
        character = gameObject.GetComponent<Character>();
    }

    void Start()
    {
        MMEventManager.AddListener<MMDamageTakenEvent>(this);
        MMEventManager.AddListener<CharacterMovementEvent>(this);
    }

}
