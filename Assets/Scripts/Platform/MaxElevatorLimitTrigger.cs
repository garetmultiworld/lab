﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxElevatorLimitTrigger : TriggerInterface
{
    public Elevator elevator;
    public int MaxIndexLimit;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        elevator.MaxIndexLimit = MaxIndexLimit;
    }

    public override void Cancel()
    {
    }
}
