﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinElevatorLimitTrigger : TriggerInterface
{

    public Elevator elevator;
    public int MinIndexLimit;

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        elevator.MinIndexLimit = MinIndexLimit;
    }

    public override void Cancel()
    {
    }
}
