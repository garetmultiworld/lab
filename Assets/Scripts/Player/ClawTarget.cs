﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClawTarget : MonoBehaviour
{

    public TriggerInterface TriggerOnGrab;
    public TriggerInterface TriggerOnRelease;
    public bool CanBeGrabbed=true;

    public void Grabbed()
    {
        if (TriggerOnGrab!=null)
        {
            TriggerOnGrab.Trigger();
        }
    }

    public void Released()
    {
        if (TriggerOnRelease != null)
        {
            TriggerOnRelease.Trigger();
        }
    }
}
