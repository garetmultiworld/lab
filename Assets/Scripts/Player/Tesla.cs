﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tesla : MonoBehaviour
{
    
    public enum TeslaForms
    {
        Melee,
        Grua
    }

    public TeslaForms CurrentForm=TeslaForms.Melee;
    public TriggerInterface TriggerMeleeForm;
    public TriggerInterface TriggerGruaForm;

    public bool GruaEnabled = false;

    public void NextForm()
    {
        switch (CurrentForm)
        {
            case TeslaForms.Melee:
                if (GruaEnabled)
                {
                    CurrentForm = TeslaForms.Grua;
                    if (TriggerGruaForm != null)
                    {
                        TriggerGruaForm.Trigger();
                    }
                }
                break;
            case TeslaForms.Grua:
                CurrentForm = TeslaForms.Melee;
                if (TriggerMeleeForm != null)
                {
                    TriggerMeleeForm.Trigger();
                }
                break;
        }
    }

}
