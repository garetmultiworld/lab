﻿using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;
using UnityEngine.Animations;

public class TeslaClaw : MonoBehaviour
{

    public enum State
    {
        Idle,
        Shooting,
        Returning,
        Grabbing
    }

    public GameObject Chie;
    public string ShootInputName;
    public string RotateInputName;
    public string DistanceInputName;
    public float ShootingSpeed=1;
    public float Reach = 10;
    public float ReturningSpeed = 2;
    public float ReloadSpeed = 1;
    public float RotateSpeed = 1;
    public float MinReach = 1;
    public float DistanceChangeSpeed = 0.3f;
    public GameObject Pivot;
    public TriggerInterface TriggerOnShoot;
    public TriggerInterface TriggerOnGrab;
    public TriggerInterface TriggerOnReachEnd;
    public TriggerInterface TriggerOnShootObstacle;
    public TriggerInterface TriggerOnRelease;
    public TriggerInterface TriggerOnReturn;
    public TriggerInterface TriggerOnReturnArrive;

    protected State _state;
    protected ClawTarget _target;
    protected Transform _targetParent;

    protected bool _isReloading = false;
    protected float _reloadTimer=0;

    void Update()
    {
        if (_isReloading)
        {
            _reloadTimer += Time.deltaTime;
            if (_reloadTimer >= ReloadSpeed)
            {
                _isReloading = false;
            }
        }
        switch (_state)
        {
            case State.Idle:
                Aim();
                break;
            case State.Shooting:
                Shoot();
                break;
            case State.Returning:
                Return();
                break;
            case State.Grabbing:
                Grab();
                break;
        }   
    }

    protected void Reload()
    {
        _isReloading = true;
        _reloadTimer = 0;
    }

    protected void Aim()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 5.23f;
        Vector3 objectPos = Camera.main.WorldToScreenPoint(transform.position);
        mousePos.x -= objectPos.x;
        mousePos.y -= objectPos.y;
        if (Chie.transform.localScale.x < 0)
        {
            mousePos.x *= -1;
            mousePos.y *= -1;
        }
        float angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        if (Input.GetAxis(ShootInputName) != 0&&!_isReloading)
        {
            Reload();
            _state = State.Shooting;
            if (TriggerOnShoot != null)
            {
                TriggerOnShoot.Trigger();
            }
        }
    }

    public void OnTriggerEnter2D(Collider2D c)
    {
        switch (_state)
        {
            case State.Idle:
                break;
            case State.Shooting:
                _target = c.GetComponent<ClawTarget>();
                if (_target == null||!_target.CanBeGrabbed)
                {
                    if (TriggerOnShootObstacle != null)
                    {
                        TriggerOnShootObstacle.Trigger();
                    }
                    if (TriggerOnReturn != null)
                    {
                        TriggerOnReturn.Trigger();
                    }
                    _state = State.Returning;
                }
                else
                {
                    _targetParent = _target.transform.parent;
                    _target.transform.parent = transform;
                    _target.Grabbed();
                    if (TriggerOnGrab != null)
                    {
                        TriggerOnGrab.Trigger();
                    }
                    _state = State.Grabbing;
                }
                break;
            case State.Returning:
                break;
            case State.Grabbing:
                break;
        }
    }

    protected void Shoot()
    {
        transform.Translate(Vector2.right * ShootingSpeed * Time.deltaTime);
        if(Vector2.Distance(Vector2.zero, transform.localPosition) > Reach)
        {
            if (TriggerOnReachEnd != null)
            {
                TriggerOnReachEnd.Trigger();
            }
            if (TriggerOnReturn != null)
            {
                TriggerOnReturn.Trigger();
            }
            _state = State.Returning;
        }
    }

    protected void Return()
    {
        transform.Translate(Vector2.left * ReturningSpeed * Time.deltaTime);
        if (Vector2.Distance(Vector2.zero, transform.localPosition) <= 1.0)
        {
            transform.localPosition = Vector3.zero;
            if (TriggerOnReturnArrive != null)
            {
                TriggerOnReturnArrive.Trigger();
            }
            _state = State.Idle;
        }
    }

    protected void Grab()
    {
        if (Input.GetAxis(ShootInputName) != 0 && !_isReloading)
        {
            Pivot.transform.localRotation = Quaternion.identity;
            Reload();
            _target.transform.parent = _targetParent;
            _target.Released();
            _targetParent = null;
            _target = null;
            if (TriggerOnRelease != null)
            {
                TriggerOnRelease.Trigger();
            }
            if (TriggerOnReturn != null)
            {
                TriggerOnReturn.Trigger();
            }
            _state = State.Returning;
            return;
        }
        float input = Input.GetAxis(RotateInputName);
        if (input != 0)
        {
            Pivot.transform.Rotate(Vector3.forward * input * RotateSpeed * Time.deltaTime);
        }
        input = Input.GetAxis(DistanceInputName);
        if (input != 0)
        {
            Vector3 prevPos = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z);
            transform.Translate(Vector2.right * input * DistanceChangeSpeed * Time.deltaTime);
            if (Vector2.Distance(Vector2.zero, transform.localPosition) > Reach|| Vector2.Distance(Vector2.zero, transform.localPosition) < MinReach)
            {
                transform.localPosition = prevPos;
            }
        }
    }
}
