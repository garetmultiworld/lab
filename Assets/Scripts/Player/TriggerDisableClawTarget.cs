﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDisableClawTarget : TriggerInterface
{
    public ClawTarget clawTarget;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        clawTarget.CanBeGrabbed = false;
    }
}
