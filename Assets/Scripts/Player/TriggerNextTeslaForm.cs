﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerNextTeslaForm : TriggerInterface
{

    public Tesla tesla;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        tesla.NextForm();
    }
}
