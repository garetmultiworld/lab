﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{

    public LevelManager levelManager;

    public string SceneToLoad;

    public void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            levelManager.GotoLevel(SceneToLoad);
        }
    }

}
