﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableTriggerByLabel : TriggerInterface
{

    public GameObject Parent;
    public string TriggerLabel;

    public override void Cancel()
    {
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        TriggerInterface[] Triggers;
        if (Parent != null)
        {
            Triggers = Parent.GetComponents<TriggerInterface>();
        }
        else
        {
            Triggers = this.gameObject.GetComponents<TriggerInterface>();
        }
        foreach (TriggerInterface trigger in Triggers)
        {
            if (TriggerLabel.Equals(trigger.GetLabel()))
            {
                trigger.enabled = false;
            }
        }
    }
}
