﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[System.Serializable]
class TriggerFireableSequenceItem
{

    public string Label;
    public TriggerInterface TriggerToFire;

    public void Trigger()
    {
        TriggerToFire.Trigger();
    }

}
