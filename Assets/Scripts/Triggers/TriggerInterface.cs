﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TriggerInterface : MonoBehaviour
{

    [Tooltip("If this trigger is disabled it won't fire")]
    public bool IsDisabled;
    [Tooltip("The name of this trigger, doesn't have to be unique")]
    public string Label;
    [Tooltip("The maximum amount of times this trigger can be activated, 0 for unlimited")]
    public int MaxActivationTimes=0;

    protected int ActivationTimes = 0;

    protected bool CanTrigger()
    {
        if (IsDisabled || (MaxActivationTimes > 0 && ActivationTimes >= MaxActivationTimes))
        {
            return false;
        }
        ActivationTimes++;
        return true;
    }

    abstract public void Trigger();

    abstract public void Cancel();

    public string GetLabel()
    {
        return Label;
    }

}
