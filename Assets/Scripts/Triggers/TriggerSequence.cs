﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSequence : TriggerInterface
{

    public TriggerSequenceItem[] Triggers;
    protected bool IsPlaying = false;

    public override void Cancel()
    {
        IsPlaying = false;
    }

    public override void Trigger()
    {
        if (!CanTrigger())
        {
            return;
        }
        IsPlaying = true;
        StartCoroutine(SequenceFire());
    }

    private IEnumerator SequenceFire()
    {
        int itemIndex = 0;
        float time = 0;
        for (itemIndex = 0; itemIndex < Triggers.Length && IsPlaying; itemIndex++)
        {
            for (time = 0; time < Triggers[itemIndex].TimeBefore && IsPlaying; time += Time.deltaTime)
            {
                yield return null;
            }
            if (IsPlaying)
            {
                Triggers[itemIndex].Trigger();
            }
            for (time = 0; time < Triggers[itemIndex].TimeAfter && IsPlaying; time += Time.deltaTime)
            {
                yield return null;
            }
        }
    }

}
