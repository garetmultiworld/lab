﻿using MoreMountains.CorgiEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogo : MonoBehaviour
{

    void Start()
    {
        DisableAll();
    }

    private void DisableAll()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    public void SetUp(DialogManager dialogManager, DialogInteractionItem dialogInteractionItem)
    {
        DisableAll();
        Transform background= transform.Find("Bg" + dialogInteractionItem.Type);
        if (background != null)
        {
            background.gameObject.SetActive(true);
        }
        Transform text = transform.Find("Txt" + dialogInteractionItem.Type);
        if (text != null)
        {
            text.gameObject.SetActive(true);
            text.GetComponent<Text>().text = I18nManager.Instance.GetFolderItem(dialogInteractionItem.I18nFolder,dialogInteractionItem.I18nItem);
        }
        MoveToPlace(dialogManager.GetType(dialogInteractionItem.Type),dialogInteractionItem);
    }

    protected void MoveToPlace(DialogType type, DialogInteractionItem dialogInteractionItem)
    {
        switch (type.locationType)
        {
            case DialogType.LocationType.Object:
                PlaceUnder(dialogInteractionItem.Parent.transform,type.RelativePosition,type.ParentName);
                break;
            case DialogType.LocationType.Player:
                PlaceUnder(LevelManager.Instance.Players[0].transform, type.RelativePosition, type.ParentName);
                break;
            case DialogType.LocationType.Random:
                gameObject.transform.parent = null;
                Vector2 topLeft = dialogInteractionItem.Bounds.GetTopLeft();
                Vector2 bottomRight = dialogInteractionItem.Bounds.GetBottomRight();
                transform.position = new Vector3(
                    UnityEngine.Random.Range(topLeft.x, bottomRight.x),
                    UnityEngine.Random.Range(topLeft.y, bottomRight.y),
                    type.RelativePosition.z
                );
                break;
        }
    }

    protected void PlaceUnder(Transform parent,Vector3 relativePosition,string ParentName)
    {
        if (ParentName!=null&&!ParentName.Trim().Equals(""))
        {
            transform.parent = TransformDeepChildExtension.FindDeepChild(parent,ParentName);
        }
        else
        {
            transform.parent = parent;
        }
        transform.localPosition = relativePosition;
    }
}
