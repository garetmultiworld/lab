﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class I18nItem
{
    public string Name;
    public string Spanish="";
    public string English="";

    public string GetI18n(string name, I18nLanguages CurrentLanguage)
    {
        switch (CurrentLanguage)
        {
            case I18nLanguages.English:
                return English;
            case I18nLanguages.Spanish:
                return Spanish;
        }
        return "";
    }

}
