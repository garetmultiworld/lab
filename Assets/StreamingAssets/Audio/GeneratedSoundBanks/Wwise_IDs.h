/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AREALOOP = 2018945488U;
        static const AkUniqueID EVIL_DOOR = 2698255118U;
        static const AkUniqueID PUERTA = 3831537568U;
        static const AkUniqueID REACTOR = 2204642885U;
        static const AkUniqueID SWORD = 2454616260U;
        static const AkUniqueID TESLAVOZ = 3376857143U;
        static const AkUniqueID WOBBLE = 4062477502U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace HEALTH
        {
            static const AkUniqueID GROUP = 3677180323U;

            namespace SWITCH
            {
                static const AkUniqueID FULL = 2510516222U;
                static const AkUniqueID LOW = 545371365U;
                static const AkUniqueID MED = 981339021U;
            } // namespace SWITCH
        } // namespace HEALTH

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID LOBBY = 290285391U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
